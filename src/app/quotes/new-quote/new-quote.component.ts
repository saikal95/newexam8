import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Quote} from "../../shared/quote.model";

@Component({
  selector: 'app-new-quote',
  templateUrl: './new-quote.component.html',
  styleUrls: ['./new-quote.component.css']
})
export class NewQuoteComponent implements OnInit{
  author!: string;
  quote!: string;
  category!: string;
  postId!: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private http: HttpClient){}

 ngOnInit() {
   this.route.params.subscribe((params: Params) => {
     this.postId = (params['id']);

     if (this.postId) {
       this.http.get<Quote>(`https://another-plovo-default-rtdb.firebaseio.com/Quotes/${this.postId}.json`)
         .pipe(map(result => {
           return new Quote(
             result.id,
             result.author,
             result.category,
             result.quote,
           );
         }))
         .subscribe(quote => {
           this.author = quote.author;
           this.category = quote.category;
           this.quote = quote.quote;
           this.postId = quote.id;
         });
     }
   })


 }


  sendPost() {
    const author = this.author;
    const quote = this.quote;
    const category = this.category;

    const body = {author, category, quote};
    this.http.post('https://another-plovo-default-rtdb.firebaseio.com/Quotes.json', body).subscribe();
  }
}
