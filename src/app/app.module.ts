import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from "@angular/forms";
import { QuotesComponent } from './quotes/quotes.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { QuoteDetailsComponent } from './quotes/quote-details/quote-details.component';
import { NewQuoteComponent } from './quotes/new-quote/new-quote.component';
import {HttpClientModule} from "@angular/common/http";
import { OneQuoteComponent } from './quotes/one-quote/one-quote.component';

@NgModule({
  declarations: [
    AppComponent,
    QuotesComponent,
    ToolbarComponent,
    QuoteDetailsComponent,
    NewQuoteComponent,
    OneQuoteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
