import {Component, Input, OnInit} from '@angular/core';
import {Quote} from "../../shared/quote.model";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-one-quote',
  templateUrl: './one-quote.component.html',
  styleUrls: ['./one-quote.component.css']
})
export class OneQuoteComponent implements OnInit {

  @Input() quote!: Quote;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
  }

}
