import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css']
})
export class QuotesComponent implements OnInit {

  quotesPlatform= ['All','star-wars', 'famous-people', 'saying', 'humour', 'motivational'];
  constructor() { }

  ngOnInit(): void {
  }

}
