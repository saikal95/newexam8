import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppComponent} from "./app.component";
import {QuotesComponent} from "./quotes/quotes.component";
import {NewQuoteComponent} from "./quotes/new-quote/new-quote.component";
import {QuoteDetailsComponent} from "./quotes/quote-details/quote-details.component";

const routes: Routes = [
  {path: 'quotes/new', component: NewQuoteComponent},
  {path: 'quotes', component: QuotesComponent, children: [
      {path: ':id', component:QuoteDetailsComponent},
      {path: ':id/:edit', component: NewQuoteComponent}
    ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
