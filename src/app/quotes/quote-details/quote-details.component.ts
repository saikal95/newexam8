import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Quote} from "../../shared/quote.model";


@Component({
  selector: 'app-quote-details',
  templateUrl: './quote-details.component.html',
  styleUrls: ['./quote-details.component.css']
})
export class QuoteDetailsComponent implements OnInit {
  quotes!: Quote[];
  quote!: Quote;
  quoteId!: string;
  platformName!: string
  urlNames!: string;
  name!: string;

  author!: string;
  category!: string;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.quoteId = params['id'];
      if (this.quoteId === 'All') {
        this.urlNames = 'https://another-plovo-default-rtdb.firebaseio.com/Quotes.json';
      } else {
        this.urlNames = `https://another-plovo-default-rtdb.firebaseio.com/Quotes.json?orderBy="category"&equalTo="${this.quoteId}"`;
      }
      this.http.get<{ [id: string]: Quote }>(this.urlNames)
        .pipe(map(result => {
          return Object.keys(result).map(id => {
            const quote = result[id];
            return new Quote(id, quote.author, quote.category, quote.quote);
          });
        }))
        .subscribe(quotes => {
          this.quotes = quotes;
        });
    });
  }

  deleteQuote(index: string) {
    this.http.delete<Quote>(`https://another-plovo-default-rtdb.firebaseio.com/Quotes/${index}.json`)
      .subscribe();
  }

  editPost(index: string) {
    void this.router.navigate(['/quotes', index, '/edit']);
    const id = this.quoteId;
    const author = this.author;
    const category = this.category;
    const quote = this.quote;
    const body = {id, author, category, quote};
    if (this.quoteId) {
      this.http.put(`https://another-plovo-default-rtdb.firebaseio.com/Quotes/${this.quoteId}.json`, body)
        .subscribe();
    } else {
      alert('Needs to be created');
    }
  }


}


