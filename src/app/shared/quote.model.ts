export class Quote {
  constructor(
    public id: string,
    public author: string,
    public category: string,
    public quote: string,
  ) {}
}
